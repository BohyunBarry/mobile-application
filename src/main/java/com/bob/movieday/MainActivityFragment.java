package com.bob.movieday;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bob.movieday.Calendar.CalendarSet;
import com.bob.movieday.Download.Download;
import com.bob.movieday.GoogleMap.GoogleMapping;
import com.bob.movieday.Video.VideoWatch;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class MainActivityFragment extends Fragment implements View.OnClickListener{
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.button01:
                ((MainActivity) getActivity()).showFragment( new GoogleMapping() );
                break;
            case R.id.button02:
                ((MainActivity) getActivity()).showFragment( new CalendarSet() );
                break;
            case R.id.button03:
                ((MainActivity) getActivity()).showFragment( new VideoWatch() );
                break;
            case R.id.button04:
                ((MainActivity) getActivity()).showFragment( new Download() );
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate( R.layout.fragment_main, container, false );
        view.findViewById( R.id.button01 ).setOnClickListener( this );
        view.findViewById( R.id.button02 ).setOnClickListener( this );
        view.findViewById( R.id.button03 ).setOnClickListener( this );
        view.findViewById( R.id.button04 ).setOnClickListener( this );

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).disableNavigationIcon();
        ((MainActivity) getActivity()).setToolbarTitle( R.string.MainFragmentTitle );
    }
}
