package com.bob.movieday.Download;

import com.google.gson.annotations.SerializedName;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class ServerImageObject {
    @SerializedName("success")
    private String success;
    public ServerImageObject(String success){
        this.success = success;
    }
    public String getSuccess() {
        return success;
    }
    public void setSuccess(String success) {
        this.success = success;
    }
}

