package com.bob.movieday.Download;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.bob.movieday.R;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class DownloadMainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        Button downloadImageButton = (Button)findViewById( R.id.download_image);
        Button uploadImageButton = (Button)findViewById(R.id.upload_image);
        Button listImageButton = (Button)findViewById(R.id.list_image);
        assert downloadImageButton != null;
        downloadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent downloadIntent = new Intent(DownloadMainActivity.this, DownloadImageActivity.class);
                startActivity(downloadIntent);
            }
        });
        assert uploadImageButton != null;
        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent uploadImageIntent = new Intent(DownloadMainActivity.this, UploadImageActivity.class);
                startActivity(uploadImageIntent);
            }
        });
        assert listImageButton != null;
        listImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listImageIntent = new Intent(DownloadMainActivity.this, ListImagesActivity.class);
                startActivity(listImageIntent);
            }
        });
    }
}
