package com.bob.movieday.GoogleMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.bob.movieday.MainActivity;
import com.bob.movieday.R;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class GoogleMapping extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Intent mIntent = new Intent( (MainActivity) getActivity(), GoogleMainActivity.class);
        ((MainActivity) getActivity()).startActivity( mIntent );
    }
    @Override
    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).enableNavigationIcon();
        ((MainActivity) getActivity()).setToolbarTitle( R.string.Fragment01Title );
    }
}
