package com.bob.movieday.Calendar.weather;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class WeatherSyncAlarmReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        startWakefulService(context, new Intent(context, WeatherSyncService.class));
    }
}
