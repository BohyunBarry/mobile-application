package com.bob.movieday;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.bob.movieday.Login.LoginActivity;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        //UI
        toolbar = (Toolbar) findViewById( R.id.toolbar );
        setSupportActionBar( toolbar );
        showFragment(new MainActivityFragment());

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    protected void showFragment(Fragment fragment) {
        String TAG = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        fragmentTransaction.replace( R.id.mainContainer, fragment, TAG );
        fragmentTransaction.addToBackStack( null );
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void backstackFragment(){
        Log.d("Stack count", getSupportFragmentManager().getBackStackEntryCount() + "");

        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
        getSupportFragmentManager().popBackStack();
        removeCurrentFragment();
    }

    private void removeCurrentFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment currentFrag = getSupportFragmentManager().findFragmentById( R.id.mainContainer );
        if(currentFrag != null){
            transaction.remove( currentFrag );
        }
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();;
        if(getSupportFragmentManager().getBackStackEntryCount() == 0){
            finish();
        }
    }

    public void enableNavigationIcon(){
        toolbar.setNavigationIcon( R.mipmap.back_arrow );
        toolbar.setNavigationOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                backstackFragment();
            }
        } );
    }

    protected void disableNavigationIcon(){
        toolbar.setNavigationIcon( null );
    }

    public void setToolbarTitle(int resID){
        toolbar.setTitle( resID );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.menu_main, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected( item );
    }
}
