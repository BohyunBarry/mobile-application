package com.bob.movieday.Video.basic.modules;

import java.lang.annotation.Retention;
import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */
@Scope
@Retention(RUNTIME)
public @interface PerActivity {
}
