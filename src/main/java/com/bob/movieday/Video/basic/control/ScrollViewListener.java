package com.bob.movieday.Video.basic.control;

import android.widget.ScrollView;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface ScrollViewListener {
    void onScrollChanged(ScrollView scrollView, int x, int y, int oldx, int oldy);
}
