package com.bob.movieday.Video.basic.mvp.view;

import com.bob.movieday.Video.model.entity.Movie;

import java.util.List;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface MoviesView extends MVPView{
    void showMovies(List<Movie> movieList);
    void showLoading ();
    void hideLoading ();
    void showLoadingLabel();
    void hideActionLabel ();
    boolean isTheListEmpty ();
    void appendMovies (List<Movie> movieList);
}
