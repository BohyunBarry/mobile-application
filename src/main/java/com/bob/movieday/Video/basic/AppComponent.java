package com.bob.movieday.Video.basic;

import com.bob.movieday.Video.basic.modules.ApplicationModule;
import com.bob.movieday.Video.basic.modules.DomainModule;
import com.bob.movieday.Video.model.rest.RestMovieSource;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        DomainModule.class,
})
public interface AppComponent {
    Bus bus();
    RestMovieSource restMovieSource();
}
