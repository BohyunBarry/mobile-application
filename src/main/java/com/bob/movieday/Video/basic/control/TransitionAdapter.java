package com.bob.movieday.Video.basic.control;

import android.transition.Transition;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class TransitionAdapter implements Transition.TransitionListener{
    @Override
    public void onTransitionStart(Transition transition) {

    }

    @Override
    public void onTransitionEnd(Transition transition) {

    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}