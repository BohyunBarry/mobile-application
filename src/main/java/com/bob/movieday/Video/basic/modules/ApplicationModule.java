package com.bob.movieday.Video.basic.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import android.content.Context;

import com.bob.movieday.Video.basic.MovieApp;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@Module
public class ApplicationModule {
    private final MovieApp application;

    public ApplicationModule(MovieApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext () { return application; }

    public static class MovieUsecasesModule {
    }
}
