package com.bob.movieday.Video.basic;

import com.bob.movieday.Video.MovieDetailActivity;
import com.bob.movieday.Video.basic.modules.MovieUsecasesModule;
import com.bob.movieday.Video.basic.modules.PerActivity;

import dagger.Component;
/**
 * Created by BohyunBartowski on 12/12/2017 0012.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = MovieUsecasesModule.class)
public interface MovieUsecasesComponent {
    void inject (MovieDetailActivity movieDetailActivity);
}
