package com.bob.movieday.Video.basic.utils;

import android.view.View;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface RecyclerViewClickListener {
    public void onClick(View v, int position, float x, float y);
}
