package com.bob.movieday.Video.basic.modules;

import com.bob.movieday.Video.model.rest.RestMovieSource;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@Module
public class DomainModule {
    @Provides
    @Singleton
    Bus provideBus () {
        return new Bus();
    }

    @Provides
    @Singleton
    RestMovieSource provideDataSource (Bus bus) { return new RestMovieSource(bus); }
}