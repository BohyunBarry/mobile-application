package com.bob.movieday.Video.basic;

import com.bob.movieday.Video.MoviesActivity;
import com.bob.movieday.Video.basic.modules.BasicMoviesUsecasesModule;
import com.bob.movieday.Video.basic.modules.PerActivity;

import dagger.Component;
/**
 * Created by BohyunBartowski on 12/12/2017 0012.
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = BasicMoviesUsecasesModule.class)
public interface BasicMoviesUsecasesComponent {
    void inject (MoviesActivity moviesActivity);
}
