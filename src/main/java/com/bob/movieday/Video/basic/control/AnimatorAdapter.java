package com.bob.movieday.Video.basic.control;

import android.animation.Animator;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class AnimatorAdapter implements Animator.AnimatorListener{
    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
