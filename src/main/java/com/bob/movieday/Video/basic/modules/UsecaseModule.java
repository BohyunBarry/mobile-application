package com.bob.movieday.Video.basic.modules;

import com.bob.movieday.Video.domain.ConfigurationUsecase;
import com.bob.movieday.Video.domain.ConfigurationUsecaseController;
import com.bob.movieday.Video.model.rest.RestMovieSource;
import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@Module
public class UsecaseModule {
    @Provides
    ConfigurationUsecase provideConfigurationUsecase (Bus bus, RestMovieSource restMovieSource) {
        return new ConfigurationUsecaseController(restMovieSource, bus);
    }
}
