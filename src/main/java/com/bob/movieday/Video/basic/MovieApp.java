package com.bob.movieday.Video.basic;

import android.app.Application;

import com.bob.movieday.Video.basic.modules.ApplicationModule;
import com.bob.movieday.Video.basic.modules.DomainModule;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class MovieApp extends Application {
    private AppComponent mAppComponent;

    @Override public void onCreate() {
        super.onCreate();
        this.initializeDependencyInjector();
    }

    private void initializeDependencyInjector() {
        mAppComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .domainModule(new DomainModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
