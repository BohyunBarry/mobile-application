package com.bob.movieday.Video.basic.mvp.view;

import android.graphics.Bitmap;

import com.bob.movieday.Video.model.entity.Review;

import java.util.List;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface DetailView extends MVPView{
    void showFilmCover(Bitmap bitmap);
    void setName (String title);
    void setDescription(String description);
    void setHomepage (String homepage);
    void setCompanies (String companies);
    void setTagline(String tagline);
    void finish(String cause);
    void showConfirmationView ();
    void animateConfirmationView ();
    void startClosingConfirmationView();
    void showReviews(List<Review> results);
    void showMovieImage(String url);
}
