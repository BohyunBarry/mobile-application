package com.bob.movieday.Video.basic.modules;

import com.bob.movieday.Video.domain.GetMovieDetailUsecase;
import com.bob.movieday.Video.domain.GetMovieDetailUsecaseController;
import com.bob.movieday.Video.model.rest.RestMovieSource;
import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@Module
public class MovieUsecasesModule {
    private final String movieId;

    public MovieUsecasesModule(String movieId) {
        this.movieId = movieId;
    }

    @Provides
    GetMovieDetailUsecase provideGetMovieDetailUsecase (Bus bus, RestMovieSource movieSource) {
        return new GetMovieDetailUsecaseController(movieId, bus, movieSource);
    }
}

