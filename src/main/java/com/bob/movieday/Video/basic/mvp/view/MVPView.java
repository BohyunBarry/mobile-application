package com.bob.movieday.Video.basic.mvp.view;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface MVPView {
    public android.content.Context getContext();
}
