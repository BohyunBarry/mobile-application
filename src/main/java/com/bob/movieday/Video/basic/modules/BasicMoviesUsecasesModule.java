package com.bob.movieday.Video.basic.modules;

import com.bob.movieday.Video.domain.ConfigurationUsecase;
import com.bob.movieday.Video.domain.ConfigurationUsecaseController;
import com.bob.movieday.Video.domain.GetMoviesUsecase;
import com.bob.movieday.Video.domain.GetMoviesUsecaseController;
import com.bob.movieday.Video.model.rest.RestMovieSource;
import com.squareup.otto.Bus;

import dagger.Module;
import dagger.Provides;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@Module
public class BasicMoviesUsecasesModule {
    @Provides
    ConfigurationUsecase provideConfigurationUsecase (Bus bus, RestMovieSource moviesSource) {
        return new ConfigurationUsecaseController(moviesSource, bus);
    }

    @Provides
    GetMoviesUsecase provideMoviesUsecase (Bus bus, RestMovieSource movieSource) {
        return new GetMoviesUsecaseController(movieSource, bus);
    }
}
