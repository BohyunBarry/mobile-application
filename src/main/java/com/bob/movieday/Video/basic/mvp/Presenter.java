package com.bob.movieday.Video.basic.mvp;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public abstract  class Presenter {
    public abstract void start ();
    public abstract void stop ();
}
