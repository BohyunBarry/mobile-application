package com.bob.movieday.Video.basic.mvp;

import com.bob.movieday.Video.basic.mvp.view.MoviesView;
import com.bob.movieday.Video.common.Constants;
import com.bob.movieday.Video.domain.ConfigurationUsecase;
import com.bob.movieday.Video.domain.GetMoviesUsecase;
import com.bob.movieday.Video.model.entity.MoviesWrapper;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;
/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class MoviesPresenter extends Presenter{
    private final Bus mBus;
    private ConfigurationUsecase mConfigureUsecase;
    private GetMoviesUsecase mGetPopularShows;
    private MoviesView mMoviesView;

    private boolean isLoading = false;
    private boolean mRegistered;

    @Inject
    public MoviesPresenter(ConfigurationUsecase configurationUsecase, GetMoviesUsecase getMoviesUsecase, Bus bus) {
        mConfigureUsecase   = configurationUsecase;
        mGetPopularShows    = getMoviesUsecase;
        mBus = bus;
    }

    public void attachView (MoviesView moviesView) {
        mMoviesView = moviesView;
    }

    @Subscribe
    public void onPopularMoviesReceived(MoviesWrapper moviesWrapper) {
        if (mMoviesView.isTheListEmpty()) {
            mMoviesView.hideLoading();
            mMoviesView.showMovies(moviesWrapper.getResults());
        } else {
            mMoviesView.hideActionLabel();
            mMoviesView.appendMovies(moviesWrapper.getResults());
        }
        isLoading = false;
    }

    @Subscribe
    public void onConfigurationFinished (String baseImageUrl) {
        Constants.BASIC_STATIC_URL = baseImageUrl;
        mGetPopularShows.execute();
    }

    public void onEndListReached () {
        mGetPopularShows.execute();
        mMoviesView.showLoadingLabel ();
        isLoading = true;
    }

    @Override
    public void start() {
        if (mMoviesView.isTheListEmpty()) {
            mBus.register(this);
            mRegistered = true;
            mMoviesView.showLoading();
            mConfigureUsecase.execute();
        }
    }

    @Override
    public void stop() {
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

}

