package com.bob.movieday.Video.model.entity;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class Production_companies {
    private Number id;
    private String name;

    public Number getId() {
        return this.id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
