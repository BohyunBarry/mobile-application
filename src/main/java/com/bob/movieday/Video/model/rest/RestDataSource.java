package com.bob.movieday.Video.model.rest;

import com.bob.movieday.Video.model.MediaDataSource;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface RestDataSource extends MediaDataSource {
    public void getMoviesByPage (int page);
}
