package com.bob.movieday.Video.model;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface MediaDataSource {
    void getMovies();
    void getDetailMovie (String id);
    void getReviews (String id);
    void getConfiguration ();
    void getImages (String movieId);
}
