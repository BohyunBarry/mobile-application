package com.bob.movieday.Video.model.entity;

/**
 * Created by BohyunBartowski on 12/12/2017 0012.
 */

public class Genres {
    private Number id;
    private String name;

    public Number getId(){
        return this.id;
    }
    public void setId(Number id){
        this.id = id;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }
}
