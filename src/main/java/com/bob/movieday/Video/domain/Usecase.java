package com.bob.movieday.Video.domain;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public interface Usecase {
    void execute ();
}
