package com.bob.movieday.Video.domain;

import com.bob.movieday.Video.model.entity.ConfigurationResponse;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@SuppressWarnings("UnusedDeclaration")
public interface ConfigurationUsecase extends Usecase{
    void requestConfiguration ();
    void onConfigurationReceived (ConfigurationResponse configurationResponse);
    void configureImageUrl (ConfigurationResponse configurationResponse);
    void sendConfiguredUrlToPresenter(String url);
}