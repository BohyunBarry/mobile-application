package com.bob.movieday.Video.domain;

import com.bob.movieday.Video.model.entity.ImagesWrapper;
import com.bob.movieday.Video.model.entity.MovieDetail;
import com.bob.movieday.Video.model.entity.ReviewsWrapper;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@SuppressWarnings("UnusedDeclaration")
public interface GetMovieDetailUsecase extends Usecase{
    void requestMovieDetail (String movieId);
    void requestMovieReviews (String movieId);
    void requestMovieImages(String movieId);
    void onMovieDetailResponse (MovieDetail response);
    void onMovieReviewsResponse (ReviewsWrapper reviewsWrapper);
    void onMovieImagesResponse (ImagesWrapper imageWrapper);
    void sendDetailMovieToPresenter (MovieDetail response);
}
