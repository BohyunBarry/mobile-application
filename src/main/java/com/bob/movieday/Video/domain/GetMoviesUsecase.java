package com.bob.movieday.Video.domain;

import com.bob.movieday.Video.model.entity.MoviesWrapper;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

@SuppressWarnings("UnusedDeclaration")
public interface GetMoviesUsecase extends Usecase{
    public void requestPopularMovies();
    public void sendMoviesToPresenter (MoviesWrapper response);
    public void unRegister ();
}
