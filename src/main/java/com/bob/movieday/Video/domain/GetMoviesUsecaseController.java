package com.bob.movieday.Video.domain;

import com.bob.movieday.Video.model.entity.MoviesWrapper;
import com.bob.movieday.Video.model.rest.RestDataSource;
import com.squareup.otto.Bus;

import javax.inject.Inject;

/**
 * Created by BohyunBartowski on 12/11/2017 0011.
 */

public class GetMoviesUsecaseController implements GetMoviesUsecase{
    private final RestDataSource mDataSource;
    private final Bus mUiBus;
    private int mCurrentPage = 1;

    @Inject
    public GetMoviesUsecaseController(RestDataSource dataSource, Bus uiBus) {
        mDataSource = dataSource;
        mUiBus = uiBus;
        mUiBus.register(this);
    }

    @Override
    public void requestPopularMovies() {
        mDataSource.getMoviesByPage(mCurrentPage);
    }

    @Override
    public void sendMoviesToPresenter (MoviesWrapper response) {
        mUiBus.post(response);
    }

    @Override
    public void unRegister() {
        mUiBus.unregister(this);
    }

    @Override
    public void execute() {
        requestPopularMovies();
        mCurrentPage++;
    }
}

